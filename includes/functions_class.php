<?php

class cw_theme {
  public $prefix; // Префикс. Используется для назаваний настроек и прочего чтобы избежать накладок с другими данными
  public $themedir; // Путь URI к текущей теме
  public $themepath; // абсолютный локальный путь к текущей теме
  public $registered_posts; // Какие посты создает тема
  public $themename; // Название темы (для пункта меню в админке)
  private $be_menu; // меню в бэкенде

  public function __construct($themename) {
    if (!function_exists('get_plugins')) {
      require_once ABSPATH . 'wp-admin/includes/plugin.php';
    }
    $this->prefix = 'iwtech_theme'; // устанавливаем префикс
    $this->themedir = get_stylesheet_directory_uri(); // устанавливаем путь к теме
    $this->themepath = preg_replace('/includes\Z/', '', dirname(__FILE__));
    $this->themename = $themename;
    if (isset($_GET['clear_fields']) && is_admin()) {
      $this->delete_option('custom_fields');
      $this->delete_option('sitevars');
      $this->delete_option('sidebars');
      $this->delete_option('styles');
      $this->delete_option('fonts');
      $this->delete_option('menu_defaults');
    }
    $this->init_options();
    add_action('get_header', array(&$this, 'basic_styles'),20); // подключаем стили
    add_action('get_footer', array(&$this, 'basic_scripts'),20); // подключаем базовые скрипты и стили, которые требуются почти всегда (jquery, стиль темы и т.д.)
    $this->init_thumbs(); // функция, включающая размеры картинок для темы
    $this->init_menus(); // включаем меню
    add_action('init', array($this, 'init_cpt'));
    $this->init_plugins(); // проверяем, все ли плагины есть
    $this->init_sliders(); // подключаем в админке настройки слайдеров
    $this->init_sitevars();
    $this->init_sidebars();
    add_filter('script_loader_src', array($this,'scripts_remove_tails'),15,1);
    add_filter('style_loader_src', array($this,'scripts_remove_tails'),15,1);
    if ($this->get_option('attachments')) {
      add_filter('attachments_default_instance', '__return_false');
      add_action('attachments_register', array($this, 'init_attachments'));
    }
    if ($this->get_option('custom_fields')) { // подключаем в админке произвольные поля для определенных записей
      add_action('add_meta_boxes', array($this, 'init_fields'), 10, 2);
      add_action('save_post', array($this, 'be_save_custom_fields'));
    }
    if (isset($this->dash_notice)) { // если какая-то функция передает сообщение админу - выводим её
      add_action('admin_notices', array(&$this, 'dash_notices'));
    }
    add_action('admin_menu', array($this, 'be_menu'));
    add_action('admin_enqueue_scripts', array($this, 'be_scripts'));
    add_action('post_gallery', array($this, 'gallery'), 10, 4);
    add_filter('pre_get_posts', array($this, 'be_archive_posts_numbers'));
    add_filter('pre_get_posts', array($this, 'be_custom_posts_query'));
  }

  public function init_sliders() {
    $sliders = $this->get_option('slider');
    if (!empty($sliders)) {
      add_theme_support('post-thumbnails');
      foreach ($sliders as $key => $value) {
        $this->register_cpt('' . $key, array('show_in_menu' => false, 'supports' => array('title', 'editor', 'excerpt', 'thumbnail',),
        'with_front' => $value['withfront'],
        'publicly_queryable' => $value['withfront'],
        'public' => $value['withfront'],
        'show_in_admin_bar' => $value['withfront'],
        'has_archive' => $value['withfront']
      ), $this->sanitize($key)
    );
  }
}
}

public function init_sitevars() {
  $sitevars = $this->get_option('sitevars');
  if ($sitevars) {
    foreach ($sitevars as $key => $value) {
      register_setting($this->prefix . '-settings-group', $key);
      add_shortcode($value['name'], array($this, 'content_sc'));
    }
  }
}

public function init_sidebars(){
  $sidebars=$this->get_option('sidebars');
  if(!is_array($sidebars)){
    return;
  }
  add_theme_support('widgets');
  add_action( 'widgets_init',function ($sidebars) use ($sidebars){
    foreach (array_values($sidebars) as $i=>$sidebar){
      register_sidebars($i+1,$sidebar);
    }
  });
}

public function init_cpt() {
  $support=$this->get_option('cpt_thumb_support');
  $cpt = $this->get_option('cpt');
  if (!($cpt)) {
    return;
  }
  foreach ($cpt as $key => $v) {
    if(isset($support[$this->sanitize($key)])){
      $v['supports'][]='thumbnail';
    }
    register_post_type($this->sanitize($key), $v);
  }
}

public function init_attachments($attachments) {
  $att = $this->get_option('attachments');
  foreach ($att as $key => $value) {
    $fields = array(
      array(
        'name' => 'title', // unique field name
        'type' => 'text', // registered field type
        'label' => __('Title', 'attachments'), // label to display
        'default' => 'title',
      ),
      array(
        'name' => 'caption', // unique field name
        'type' => 'textarea', // registered field type
        'label' => __('Caption', 'attachments'), // label to display
        'default' => 'caption', // default value upon selection
      ),
    );
    if ($value['extra']) {
      foreach ($value['extra'] as $field) {
        $fields[] = array(
          'name' => $field,
          'type' => 'text',
          'label' => __($field),
          'default' => ''
        );
      }
    }
    $args = array(// title of the meta box (string)
      'label' => $value['title'],
      // all post types to utilize (string|array)
      //    'post_type' => array('post', 'page'),
      'post_id' => array('post_id'),
      // meta box position (string) (normal, side or advanced)
      'position' => 'normal',
      // meta box priority (string) (high, default, low, core)
      'priority' => 'high',
      // allowed file type(s) (array) (image|video|text|audio|application)
      'filetype' => null, // no filetype limit
      // include a note within the meta box (string)
      'note' => _('Attach files here!'),
      // by default new Attachments will be appended to the list
      // but you can have then prepend if you set this to false
      'append' => true,
      // text for 'Attach' button in meta box (string)
      'button_text' => __('Attach Files', 'attachments'),
      // text for modal 'Attach' button (string)
      'modal_text' => __('Attach', 'attachments'),
      // which tab should be the default in the modal (string) (browse|upload)
      'router' => 'browse',
      // whether Attachments should set 'Uploaded to' (if not already set)
      'post_parent' => false,
      // fields array
      'fields' => $fields,
    );
    if (!$value['notsingle']) {

      if ((is_admin() && $_GET['post'] == $value['post_id'] && $_GET['action'] == 'edit') || (!is_admin())) {


        ($attachments->register($key, $args));
      }
    } else {
      //$args['post_type']
      $args['post_type'] = $value['post_type'];
      ($attachments->register($key, $args));
    }
  }
}

public function init_options() {
  $args = array('variant' => 'true_or_false');
  if ($this->sitevar('Отключить комментарии', $args)) {
    $this->disable_comments();
  }
}

public function clear_spaces($value) {
  $clean = array("\n", "\t", "\r");
  $val = str_replace($clean, '', $value);
  return trim($val);
}

public function df($name, $value = false, $ft = 'textarea') {
  if (!$value) {
    $value = $name;
  }
  $field = $this->field($name, array('notsingle' => false, 'variant' => $ft));
  if (strlen($field)) {
    return $this->clear_spaces($field);
  }
  $fieldkey = $this->sanitize($name) . '__' . (int) get_the_ID();
  update_post_meta(get_the_ID(), $fieldkey, $this->clear_spaces($value));
  return $this->clear_spaces($value);
}

public function content_sc($atts, $content, $tag) {
  return get_option($this->sanitize($tag));
}

public function gallery($output = '', $atts, $content = false, $tag = false) {
  $this->include_block('gallery/' . get_post_type($this->get_the_pid()), $atts);
  return ' ';
}

public function init_fields($post_type, $post) {
  $fields = $this->get_option('custom_fields');
  if (!$fields) {
    return;
  }
  foreach ($fields as $key => $box) {
    if(true!==$box['hidden']){
      if ($box['notsingle'] && $box['post_type'] == $post_type) {
        add_meta_box(
        $this->sanitize('custom fields post type ' . $key), __($box['label']), array($this, 'be_custom_field'), $post_type, 'normal', 'low', array('name' => $key, 'value' => $box)
      );
    }
    if (!$box['notsingle'] && $post->ID == $box['post_id']) {
      add_meta_box(
      $this->sanitize('custom fields post type ' . $key), __($box['label']), array($this, 'be_custom_field'), $post_type, 'normal', 'low', array('name' => $key, 'value' => $box)
    );
  }}
}
}

public function be_custom_posts_query($query) {
  if (is_tax() && $query->is_main_query()) {
    $pts = array();
    if (!empty($query->query_vars['post_type'])) {
      $pts = ($query->query_vars['post_type']);
    }
    $cpt = $this->get_option('cpt');
    $pts[] = 'post';
    foreach ($cpt as $k => $pt) {
      if ($pt['public']) {
        $pts[] = $this->sanitize($k);
      }
    }
    $query->set('post_type', $pts);
  }
}

public function be_archive_posts_numbers($query) {
  if (is_tax() && $query->is_main_query()) {
    $tax_name = (get_queried_object()->taxonomy);
    $qty = $this->sitevar('!Количество записей, отображаемых для таксономии "' . $tax_name . '"');
    if (strlen($qty)) {
      $query->set('posts_per_page', $qty);
    }
    return;
  }

  $post_types = get_post_types(array(), 'objects');
  if ($query->is_archive || is_tax()) {
    foreach ($post_types as $k => $pt) {
      if (isset($query->query['post_type'])&& $query->query['post_type'] == $k) {
        $qty = $this->sitevar('Количество записей, отображаемых в архиве "' . $pt->labels->name . '"');
        if (strlen($qty)) {
          $query->set('posts_per_page', $qty);
        }
      }
    }
  }
}

public function be_scripts($hook) {
  wp_enqueue_script($this->prefix . 'be_script', $this->themedir . '/backend/js/iwt-scripts.js');
}

public function be_custom_field($post, $args) {
  $args['args']['field_value'] = htmlspecialchars(get_post_meta($post->ID, $args['args']['name'], true));
  $args['args']['field_name'] = 'iwt[form][' . $args['args']['name'] . ']';
  $this->include_block('cf/' . $args['args']['value']['variant'], $args['args']);
}

public function be_save_custom_fields($pid) {
  if (!isset($_POST['iwt']['form'])) {
    return;
  }
  $fields = ($_POST['iwt']['form']);
  foreach ($fields as $k => $v) {
    //$v = str_replace('"', '&quot;', $v);
    $v = htmlspecialchars_decode($v);
    (check_admin_referer('_' . $k, '_' . $k));
    update_post_meta($pid, $k, $v);
  }
}

public function be_page($page) {

  //$this->include_block(str_replace('iwttheme', '../', $_GET['page']));
  $pages=explode($this->prefix,$page);
  require($pages[0].'.php');
}

private function exec($param) {
  return shell_exec('cd ' . get_home_path() . '&&' . $param . ' 2>&1');
}

public function be_menu() {
  $mainitem = 'iwttheme_options';
  if (file_exists(dirname(__FILE__) . '/options.php')) {
    $class=&$this;
    add_menu_page($mainitem, _('Настройки темы'), 'manage_options', $this->prefix . 'options', function() use ($class){
      require(dirname(__FILE__) . '/options.php');
    });
    //    add_menu_page($this->themedir . "/options.php", __('Настройки темы'), 'manage_options', $mainitem, array($this, 'be_page'));
    if (file_exists(dirname(__FILE__) . '/sanitize.php')) {
      add_submenu_page($this->prefix . 'options', __('Настройки URL'), 'manage_options', $this->prefix.'sanitize', $this->be_page('sanitize'));
    }
    if (file_exists(dirname(__FILE__) . '/revisions.php')) {
      add_submenu_page($this->prefix . 'options', __('Контроль целостности'), __('Контроль целостности'),'manage_options', $this->prefix.'revisions',function() use ($cla$
        //echo 123;
        $class->be_page('revisions');
      });
    }
    ///    add_theme_page('Theme Option', 'Theme Options', 'manage_options', 'pu_theme_options.php', 'pu_theme_page'));

    if (!$this->registered_posts) {
      return;
    }
    foreach ($this->registered_posts as $key => $value) {
      //    add_submenu_page($mainitem, $value['name'], $value['name'], 5, "edit.php?post_type=" . $key);
      add_submenu_page($this->prefix.'options', $value['name'],$value['name'],'manage_options',"edit.php?post_type=" . $key);
    }
  }
}

public function fill_params($default, $params=array()) {//функция заполнения недостающих значений значениями по умолчанию
  if(!empty($params)){
     foreach ($params as $k => $v) {
        $default[$k] = $v;
      }
    }
  return $default;
}

public function register_cpt($label = false, $settings = false, $slug = false) {
  $this->post_labels = array(
    'name' => __('%post%'),
    'singular_name' => __('%post%'),
    'add_new' => __('Add') . " " . __('%post%'),
    'all_items' => __('All') . " " . __('%post%'),
    'add_new_item' => __('Add') . " " . __('%post%'),
    'edit_item' => __('Edit') . " " . __('%post%'),
    'new_item' => __('New') . " " . __('%post%'),
    'view_item' => __('View %post%'),
    'search_items' => __('Search %post%'),
    'not_found' => __('No %post%s found'),
    'not_found_in_trash' => __('No %post%s found in trash'),
    'parent_item_colon' => __('Parent %post%')
  );
  if (isset($label)) {
    foreach ($this->post_labels as $key => $value) {
      $this->post_labels[$key] = ucwords(str_replace('%post%', $label, $value));
    }
  }
  $show = false;
  if ($this->sitevar('!Запись вида ' . $label . ' поместить в админ-меню', array('variant' => 'true_or_false')) == 1) {
    $show = true;
  }
  $this->post_settings = array(
    'labels' => $this->post_labels,
    'public' => false,
    'show_ui' => true,
    'has_archive' => false,
    'show_in_menu' => $show,
    'publicly_queryable' => false,
    'query_var' => false,
    'show_in_admin_bar' => true,
    'exclude_from_search' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'supports' => array(
      'title',
      'editor',
      'revisions',
      //		'custom-fields'
    )
  );
  if (($settings)) {
    foreach ($settings as $key => $value) {
      $this->post_settings[$key] = $value;
      if ($key == 'has_archive' && $value) {
        $this->update_option('required_plugins', 'Post type archive in menu', 'Post type archive in menu');
      }
    }
  }
  if (!$slug) {
    $slug = $this->sanitize($this->post_labels['name']);
  }
  if (post_type_exists($slug)) {
    return false;
  }
  //        (register_post_type($slug, $this->post_settings));
  $this->registered_posts[$slug] = $this->post_labels;
  $this->update_option('cpt', $label, $this->post_settings);
  return true;
}

public function dash_notices() { // вывод сообщений в админке. Сообщения берутся из $this->dash_notice
  foreach ($this->dash_notice as $notice) {
    ?>
    <div class="update-nag"><?php echo $notice; ?></div>
    <?php
  }
}

private function init_plugins() { // проверяем нужные плагины
  $this->plugins = $this->get_option('required_plugins'); //они берутся из настройки 'required plugins' которая задается через публичную функцию update_option
  if (!$this->plugins) {
    return; // если таковых нет, завершаем функцию
  }
  $allplugins_ = get_plugins(); // проверяем, что установлено
  foreach ($allplugins_ as $key => $plg) {
    $allplugins[$plg['Name']] = $key; // формируем более удобный массив для проверки
  }

  foreach ($this->plugins as $value) { //проверяем каждые требуемый плагин на его наличие в массиве установленных плагинов
    if ((isset($allplugins[$value])) || (isset($allplugins[$value]) && !is_plugin_active($allplugins[$value]))) { // если нет или есть, но неактивен, дописываем в массив сообщений админу просьбу установить и активировать
      $this->dash_notice[] = (__('Пожалуйста, установите и активируйте plugin') . ' <a href="' . get_site_url() . '/wp-admin/plugin-install.php?tab=search&type=term&s=' . $value . '">' . $value . '</a>');
    }
  }
}

private function init_menus() { // функция добавления меню к теме
  $this->update_option('menu_defaults', 'fallback_cb', false);
  $this->menus = $this->get_option('menus'); // берем из настроек "menus"
  if ($this->menus) { // если таковые настройки есть, то
    add_theme_support('menus'); // добавляем в принципе поддержку меню
    foreach ($this->menus as $key => $value) { // регистрируем каждую позицию меню
      if(strlen($key)>0){
        register_nav_menu($key, $value);
      }
    }
  }
}

private function init_thumbs() { // функция добавления размеров картинок, принцип сходный с функцией init_menus()
  $this->thumbs = $this->get_option('thumbs');
  if ($this->thumbs) {
    add_theme_support('post-thumbnails');
    foreach ($this->thumbs as $key => $value) {
      add_image_size($key, $value['width'], $value['height'], $value['crop']);
    }
  }
}

public function get_thumb($args=array()) {
  // выводим картинку для поста нужного размера, одновременно параметры требуемой картинки дописываем в настройки, если таковых там нет
  // параметры:
  // width, height, crop - ширина, высота, обрезание (true/false)
  // class - класс тэга
  // pid - текущий post id
  // все параметры необязательные
  if(!isset($args['class'])){
    $args['class']='';
  }
  if(!isset($args['pid'])) {
    $pid=$this->get_the_pid();
  } else {
    $pid=$args['pid'];
  }
  if(!isset($args['alt'])){
    $args['alt']=get_the_title($pid);
  }
  $src=$this->get_thumb_src($pid,$args);
  if(strlen($src)>0){
    return '<img src="'.$this->get_thumb_src($pid,$args).'" class="'.$args['class'].'" alt="'.$args['alt'].'"/>';
  } else {
    return false;
  }
}

public function get_thumb_src($pid = false, $args1 = 'full') {
  // возвращает ссылку на миниатюру записи или картинку. По умолчанию - исходное загруженное изображение,
  // либо через второй параметр передаем массив: array('width'=>ширина, 'height'=>высотка, 'crop'=>кадрирование (true/false));
  if (!$pid) {
    $pid = $this->get_the_pid();
  }
  $this->update_option('cpt_thumb_support',get_post_type($pid),1);
  if (is_array($args1)) {
    $this->update_option('thumbs', $this->prefix . $args1['width'] . $args1['height'] . $args1['crop'], array(
      'width' => $args1['width'],
      'height' => $args1['height'],
      'crop' => $args1['crop']
    )
  );
  $args = $this->prefix . $args1['width'] . $args1['height'] . $args1['crop'];
} else {
  $args = $args1;
  $this->update_option('thumbs', $args1, true);
}
$mime = preg_replace('/\/[^\Z]*\Z/', '', get_post_mime_type($pid));
switch ($mime) {
  case 'image':
  $att = wp_get_attachment_image_src($pid, $args);
  return $att[0];
  break;
  default:
  $att = wp_get_attachment_image_src(get_post_thumbnail_id($pid), $args);
  return $att[0];
  break;
}
}

public function slider($slider, $variant = '', $withfront = false) { //выводим слайдер во фронтенде, в админке создаем настройку
  $slidername = $this->sanitize($slider);
  $args = array('post_type' => $slidername,
      'posts_per_page' => 999
  );
  query_posts($args);
  $this->update_option('slider', $slider, array('title' => 'Слайдер '.$slider, 'variant' => $variant, 'withfront' => $withfront));
  $this->include_block('slider' . $variant, $slider);
  wp_reset_query();
}

public function include_block($file, $params = false) {
  if (file_exists(dirname(__FILE__) . '/blocks/' . $file . '.php')) {
    require(dirname(__FILE__) . '/blocks/' . $file . '.php');
  } else {
    //return false;
    echo (dirname(__FILE__) . '/blocks/' . $file . '.php');
  }
}

public function menu_items($theme_location) {
  global $wp_query;
  $this->update_option('menus', ($theme_location), $theme_location);
  if(isset($wp_query->post)){
    $post = $wp_query->post;
    if(is_object($post)){
      $thePostID = $post->ID;
    } else {
      $thePostID=false;
    }}
    $array = $this->wp_get_nav_menu_items($this->get_menu_name_by_location($theme_location));
    $this_item = current( wp_filter_object_list( $array, array( 'object_id' => get_queried_object_id() ) ) );
    $tmp_ = array();
    $item_children = array();
    if ($array && !empty($array)) {
      foreach ($array as $key => $item) {
        $tmp_[$item->ID] = $key;
        if(isset($this_item) && false!=$this_item){
          if ($item->object_id == $this_item->object_id) {
            $array[$key]->classes[] = 'current';
          }
          if( $item->ID==$this_item->menu_item_parent){
            $array[$key]->classes[] = 'current-parent';
          }}
          if ($item->menu_item_parent > 0) {
            $item_children[$item->menu_item_parent][] = $item->ID;
          }
        }
      }
      if (!empty($item_children)) {
        foreach ($item_children as $k => $v) {
          $children = array();
          foreach ($v as $v1) {
            $children[$tmp_[$v1]] = $v1;
          }
          $array[$tmp_[$k]]->menu_children = $children;
        }
      }
      return $array;
    }

    public function get_menu_by_location($theme_location) {
      if (!$theme_location) {
        return false;
      }
      $theme_locations = get_nav_menu_locations();
      if (!isset($theme_locations[$theme_location])) {
        return false;
      }
      $menu_obj = get_term($theme_locations[$theme_location], 'nav_menu');
      if (!$menu_obj) {
        $menu_obj = false;
      }
      return $menu_obj;
    }

    public function get_menu_name_by_location($theme_location) {
      if (!$theme_location) {
        return false;
      }
      $theme_locations = get_nav_menu_locations();
      if (!isset($theme_locations[$theme_location])) {
        return false;
      }
      $menu_obj = get_term($theme_locations[$theme_location], 'nav_menu');
      if (!$menu_obj) {
        $menu_obj = false;
      }
      if (!isset($menu_obj->name)) {
        return false;
      }
      return $menu_obj->name;
    }

    public function menu($args) { // синоним wp_nav_menu()
      return $this->wp_nav_menu(array('theme_location' => $args));
    }

    public function wp_get_nav_menu_items($menu, $args = false) {
      $theme_location = $args['theme_location'];
      $menu_defaults = $this->get_option('menu_defaults');
      $this->update_option('menus', ($theme_location), $theme_location);
      return wp_get_nav_menu_items($menu, $args);
    }

    public function wp_nav_menu($args) { // выводим меню, а также дописываем его в настройки
      $theme_location = $args['theme_location'];
      $menu_defaults = $this->get_option('menu_defaults');
      $menu_defaults['menu_id'] = 'id' . $this->sanitize($theme_location);
      foreach ($menu_defaults as $key => $value) {
        if (!isset($args[$key])) {
          $args[$key] = $value;
        }
      }
      $this->update_option('menus', ($theme_location), $theme_location);
      $args_silent = $args;
      $args_silent['echo'] = false;
      $menu = wp_nav_menu($args_silent);
      if ((int) strlen($menu) == 0) {
        return false;
      } else {
        return $menu;
      }
    }

    public function update_option($option, $key, $value) { // функция обновления настроек. настройки формируются в виде массива. Если это не группа настроек, то имеет смысл использовать родные функции wordpress'а
      $options = $this->get_option($option);
      $options[$key] = $value;
      update_option($this->prefix . $option, serialize($options));
    }

    public function delete_option($option, $key = false) { // функция удаления группы настроек или части массива
      if ($key) {
        $options = $this->get_option($option);
        unset($options[$key]);
        update_option($this->prefix . $option, serialize($options));
      } else {
        delete_option($this->prefix . $option);
      }
    }

    public function wp_title() {
      if (function_exists('is_tag') && is_tag()) {
        echo $this->sitevar('Перевод: Tag Archive for &quot;') . $tag . '&quot; - ';
      } elseif (is_archive()) {
        wp_title('');
        echo ' Archive - ';
      } elseif (is_search()) {
        global $s;
        echo  $this->sitevar('Перевод: Search for &quot;') . esc_html($s) . '&quot; - ';
      } elseif (!(is_404()) && (is_single()) || (is_page())) {
        wp_title('');
        echo ' - ';
      } elseif (is_404()) {
        echo  $this->sitevar('Перевод: Not Found - ');
      } if (is_home()) {
        bloginfo('name');
        echo ' - ';
        bloginfo('description');
      } else {
        bloginfo('name');
      }
    }

    public function get_the_time($format, $pid = false) {
      // показываем дату записи в нужном формате по-русски, используйте формат %d, %H и т.д. Если нужен порядковый номер, используем формат %jS,%FS и т.д.
      $time = get_the_time('U', $pid);
      $FS = array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
      preg_match_all('/\%[^\%]S{0,1}/', $format, $matches);
      foreach ($matches[0] as $v) {
        $vv = str_replace('%', '', $v);
        switch ($vv) {
          case 'jS':
          $format = str_replace('%' . $vv, str_replace('th', '-е', get_the_time($vv, $pid)), $format);
          break;
          case 'FS':
          $format = str_replace('%' . $vv, $FS[get_the_time('m', $pid) - 1], $format);
          break;
          default:
          $format = str_replace('%' . $vv, get_the_time($vv, $pid), $format);
          break;
        }
      }
      return $format;
    }

    public function get_the_pid($pid=false) {
      if(is_tax()){
        //return get_query_var('taxonomy_id');
        //return false;
      }
      if (!$pid) {
        $pid = get_the_ID();
        if (!$pid) {
          global $post;
          return $post->ID;
          //   global $wp_query;
          //   $post_id = $post->ID;
          //return $post_id;
        }
      }
      return $pid;
    }

    public function field_name($name) {
      if (strpos('_' . $name, '!') != 1) {
        if ($this->get_option('multilang')) {
          $name.='|' . get_locale();
        }
      } else {
        $name = preg_replace('/\A\!/', '', $name);
      }
      return $name;
    }

    public function sitevar($name, $args = false) {
      // Общие значения для сайта, которые настраиваются в админке на странице настроек темы.
      // Например, если на сайте часто повторяется номер телефона, в теме можно прописать $theme->sitevar('Номер телефона');
      // После чего прописать его один раз в админке для всего сайта.
      // В качестве $args вместо массива можно сразу писать значение по умолчанию
      $name = $this->field_name($name);
      $default_args = array(
        'variant' => 'textarea',
        'default' => ''
      );
      if ($args) {
        if (is_array($args)) {
          foreach ($args as $k => $v) {
            $default_args[$k] = $v;
          }
        } else {
          $default_args['default'] = $args;
        }
      }
      $default_args['name'] = $name;
      $key = $this->sanitize($name);
      $this->update_option('sitevars', $key, $default_args);
      $val = get_option($key);
      if (!$val) {
        $val = $default_args['default'];
      }
      return $val;
    }

    public function attachments($params = false, $pid_ = false) {
      // Создаем и выводим прикрепленные файлы
      // Важно! Необходим плагин Attachments
      // второй параметр - вытягивание аттачей из другого поста
      $pid = $this->get_the_pid($pid);
      $type = get_post_type($pid);
      $default_args = array(//Значения по умолчанию
        'pid' => $pid,
        'notsingle' => true,
        'title' => __('Прикрепите файлы')
      );
      if (is_string($params)) {
        $params_['title'] = $params;
        $params = array();
        $params = $params_;
      }
      foreach ($params as $k => $v) {
        $default_args[$k] = $v;
      }

      $key = $this->sanitize($default_args['title']);
      if ($default_args['notsingle']) { // прикрепляем ко всем подобным записям notsingle==true
        $key = $key . $type;
        $this->update_option('attachments', $key, array(
          'title' => $default_args['title'],
          'post_type' => $type,
          'notsingle' => $default_args['notsingle'],
          'extra' => $default_args['extra']
        ));
      } else { // прикрепляем только к данной конкретной записи notsingle==false
        $key = $key . '__' . $default_args['pid'];
        $this->update_option('attachments', $key, array(
          'title' => $default_args['title'],
          'post_type' => $type,
          'notsingle' => $default_args['notsingle'],
          'post_id' => $default_args['pid'],
        ));
      }
      if (!isset($pid_)) {
        $pid = $pid_;
      }
      $files = json_decode(get_post_meta($pid, 'attachments', true), true);
      $key = str_replace('-', '_', $key);
      return ($files[$key]);
    }

    public function field($name, $args = false, $extra = false) {
      /* Создаем и выводим произвольные поля
      * (наименование поля, вид поля, запись, поле для все подобных записей (true) или только для этой
      */
      //$variant = 'textarea', $pid = false, $notsingle = true
      if (!isset($pid)) {
        $pid = false;
      }
      $name = $this->field_name($name);
      $default_args = array(
        'variant' => 'textarea',
        'pid' => $this->get_the_pid($pid),
        'notsingle' => true,
        'cloneable' => false,
        'hidden'=>false
      );

      if (is_array($args)) {
        foreach ($args as $key => $value) {
          $default_args[$key] = $value;
        }
      }
      $type = get_post_type($default_args['pid']);
      if ($default_args['notsingle']) {
        $key = $this->sanitize($name) . $type;
        $this->update_option('custom_fields', $key, array(
          'variant' => $default_args['variant'],
          'post_type' => $type,
          'notsingle' => $default_args['notsingle'],
          'label' => $name,
          'hidden'=>$default_args['hidden']

        ));
      } else {
        $key = $this->sanitize($name) . '__' . $default_args['pid'];
        $this->update_option('custom_fields', $key, array(
          'variant' => $default_args['variant'],
          'post_type' => $type,
          'notsingle' => $default_args['notsingle'],
          'post_id' => $default_args['pid'],
          'label' => $name,
          'extra' => $extra,
          'hidden'=>$default_args['hidden']
        ));
      }
      return get_post_meta($default_args['pid'], $key, true);
    }

    public function cut($pid = false) { // выводим содержимое записи до <!-- more -->
      $pid = $this->get_the_pid($pid);
      $p = get_post($pid);
      if ($p->post_excerpt) {
        return $p->post_excerpt;
      } else {
        $p_ = explode('<!--more-->', $p->post_content);
        return $p_[0];
      }
    }

    public function get_option($option, $key = false) { //функция получения настроек группы - всех или по ключу
      $option = unserialize(get_option($this->prefix . $option));
      if ($option) {
        return $option;
      } else {
        return false;
      }
    }

    public function sanitize($value, $nocache = false) { // функция для превращения строки в уникальную строку и цифру для id html-тегов
      //return 'r'.crc32($value);
      if (!$nocache) {
        $san = $this->get_option('sanitize');
        $val = $value;
        $saveme = 1;
        if (isset($san[$value])) {
          $value = $san[$value];
          $saveme = 0;
        }
      }

      // $value = mb_convert_encoding($value, 'UTF-8', $encoding);
      $out = sanitize_title($value);

      if (strlen($out) > 20) {
        $out = substr($out, 0, 9) . crc32($value);
      } else {

      }
      if ($saveme == 1) {
        $this->update_option('sanitize', $val, $out);
      }
      return $out;
    }

    public function pagination($pages = '', $range = 4) {
      $showitems = ($range * 2) + 1;
      global $paged;
      if (empty($paged)) {
        $paged = 1;
      }
      if ($pages == '') {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if (!$pages) {
          $pages = 1;
        }
        if ($pages == 1) {
          return false;
        }
      }

      for ($i = 1; $i <= $pages; $i++) {
        //      if (1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems )) {
        $out['pages'][$i] = get_pagenum_link($i);
        //    }
      }

      if ($paged < $pages && $showitems < $pages) {
        $out['next'] = get_pagenum_link($paged + 1);
      }
      if ($paged < $pages - 1 && $paged + $range - 1 < $pages && $showitems < $pages) {
        $out['last'] = get_pagenum_link($pages);
      }
      $out['previous'] = $this->ex_url(get_previous_posts_link(null));
      $out['next'] = $this->ex_url(get_next_posts_link(null));
      $out['totalpages'] = $pages;
      $out['current'] = $paged;
      $out['showitems'] = $showitems;
      $out['range'] = $range;
      return $out;
    }

    public function ex_url($string) {
      preg_match_all('/href\=\"([^\"]*)\"/', $string, $st);
      return $st[1][0];
    }

    public function find_themefile($find) { // Ищем все что имеет расширение $find, затем проверяем массив $this->loadorder[$find]. Если в нём есть значения, то загружаем сортируем в указанном в ней порядке.
      $it = new RecursiveDirectoryIterator($this->themepath);
      foreach (new RecursiveIteratorIterator($it) as $file) {
        $string = explode('.', strtolower((string) $file->getPathName()));
        if ((array_pop($string) == $find)) {
          if (!strpos((string) $file, 'backend')) {
            $filename=str_replace($this->themepath, '', $file);
            $files[$filename] = $this->themedir . '/' . $filename;
          }
        }
      }
      if(isset($this->loadorder[$find])){
        foreach ($this->loadorder[$find] as $k=>$v){
          $loadorder[$v]=$v;
        }
        foreach ($files as $key=>$value){
          $loadorder[$key]=$value;
        }
        return $loadorder;
      }
      return $files;
    }

    public function basic_scripts() { // здесь запуск разных js-скриптов и подключение css
      foreach ($this->find_themefile('js') as $file) {
        wp_enqueue_script($this->sanitize($file), $file, '', '', true);
      }
    }

    public function basic_styles(){
      if(!is_admin()){
        wp_deregister_style( 'open-sans' );
        wp_register_style( 'open-sans', false );
      }
      $styles = $this->get_option('styles'); // если в настройках заданы урлы дополнительных стилей (например google fonts), подключаем их тоже
      if ($styles) {
        $st = 0;
        foreach ($styles as $k => $v) {
          $st++;
          if((int)strlen($v)>0){
            wp_enqueue_style('style1_' . $st, $v);
          }}
        }
        $st = 0;
        foreach ($this->find_themefile('css') as $key => $style) {
          $st++;
          wp_register_style('style2_' . $st, $style, array(), '', 'all' );
          wp_enqueue_style('style2_' . $st, $style);
        }
      }

      public function scripts_remove_tails($src){
        $args = array('variant' => 'true_or_false');
        if(!$this->sitevar('Отключить get-параметры загружаемых скриптов и стилей',$args)){return $src;}
        $parts = explode( '?', $src );
        return $parts[0];
      }

      public function get_term_parents($taxonomies, $post_types, $term_id, $notfirst = false) {
        //Возвращает родителей для term_id определенной таксономии для определенных видов записей
        $terms = '';
        if (!$this->terms_structure) {
          $this->terms_structure = $this->get_terms_by_post_type($taxonomies, $post_types);
        }
        foreach ($this->terms_structure as $term) {
          if ($term->term_id == $term_id) {
            $terms.=$term_id . ',';

            $terms.=$this->get_term_parents($taxonomies, $post_types, $term->parent, true);
          }
        }
        if (!$notfirst) {
          return explode(',', $terms);
        }
        return (int) $terms;
      }

      public function get_terms_by_post_type($taxonomies, $post_types, $parent = false) {
        // Функция возвращает значения таксономии для определенных видов записей, можно сделать запрос по родителю
        if (is_string($taxonomies)) {
          $taxonomies = array($taxonomies);
        }
        if (is_string($post_types)) {
          $post_types = array($post_types);
        }
        global $wpdb;
        $where = '';
        if (strlen($parent)) {
          $where = ' AND tt.parent=' . $parent . ' ';
        }
        $query = $wpdb->prepare(
        "SELECT t.*,tt.parent, COUNT(*) as qty from $wpdb->terms AS t
        INNER JOIN $wpdb->term_taxonomy AS tt ON t.term_id = tt.term_id
        INNER JOIN $wpdb->term_relationships AS r ON r.term_taxonomy_id = tt.term_taxonomy_id
        INNER JOIN $wpdb->posts AS p ON p.ID = r.object_id
        WHERE p.post_type IN('%s') AND tt.taxonomy IN('%s') " . $where .
        "GROUP BY t.term_id", join("', '", $post_types), join("', '", $taxonomies)
      );
      $results = $wpdb->get_results($query);
      if ($results[0]->term_order) {
        return $this->sort_object($results, 'term_order');
      }
      return $results;
    }

    public function tax_menu($taxonomy, $post_type, $parent = 0) {
      //Возвращает html-меню для любой таксономии по виду записи
      $out = '';
      if (!$this->terms[$taxonomy . $post_type]) {
        $this->terms[$taxonomy . $post_type] = get_terms($taxonomy);
      }
      $terms_ = $this->get_terms_by_post_type($taxonomy, $post_type, $parent);
      foreach ($terms_ as $term) {
        $terms[$term->term_id] = $term;
      }
      foreach ($this->terms[$taxonomy . $post_type] as $term) {
        if ($term->parent == $parent && $terms[$term->term_id]) {
          $out.='<li';
          if (get_queried_object()->term_id == $term->term_id) {
            $out.=' class="current" ';
          }
          $out.='>';
          $out.="<a href=\"" . get_term_link($term) . "\" data-term=\"" . $term->term_id . '">' . $term->name . "</a>";
          $out.=$this->tax_menu($taxonomy, $post_type, $term->term_id);
          $out.='</li>';
        }
      }
      if (strlen($out)) {
        return '<ul>' . $out . '</ul>';
      }
      return false;
    }

    public function sort_object($array, $field) {
      foreach ($array as $value) {
        $arr[$value->$field][] = $value;
      }
      ksort($arr);
      foreach ($arr as $value) {
        foreach ($value as $val) {
          $sorted[] = $val;
        }
      }
      return $sorted;
    }

    public function disable_comments() { //функция отключения комментов и трекбеков
      if (function_exists('df_disable_comments_post_types_support')) {
        return;
      }
      require($this->themepath . 'includes/disable_comments.php');
    }

  }
