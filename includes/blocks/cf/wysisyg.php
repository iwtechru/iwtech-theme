<?php
$args=array(
    'wpautop' => false,
    'media_buttons' => true,
    'textarea_rows' => 20,
    'textarea_name'=>$params['field_name'],
    'tabindex' => '',
    'tabfocus_elements' => ':prev,:next',
    'editor_css' => '',
    'editor_class' => '',
    'teeny' => false,
    'dfw' => false,
    'tinymce' => true, // <-----
    'quicktags' => true
);
wp_editor( html_entity_decode($params['field_value']), $params['name'],$args);
wp_nonce_field('_'.$params['name'], '_'.$params['name']);