<?php
if(isset( $_POST['do_submit'])){
if ( wp_verify_nonce( $_POST['do_submit'], 'do_submit' ) && is_admin() ) {
	$this->exec( 'git add -A' );
	$this->exec( 'git commit -m "commited by admin"' );
}}
?>
<h2>Проверка целостности Wordpress</h2>
<?php if ( $this->sitevar( 'Создавать git-репозиторий', array( 'default' => '0', 'variant' => 'true_or_false','admin'=>1 ) ) == 1 ) { ?>
	<XMP><?php
		if ( !file_exists( get_home_path() . '/.git' ) ) {
			$this->exec( 'git init' );
			if ( !file_exists( get_home_path() . '/.git' ) ) {
				_e( 'Ошибка: Невозможно создать git-репозиторий.' );
				exit;
			}
			$this->exec( 'git config user.name "iwtech.ru theme"' );
			$this->exec( 'git config user.email "' . get_bloginfo( 'admin_email' ) . '"' );
			$this->exec( 'git add -A' );
			($this->exec( 'git commit -m "commited by theme"' ));
		}
		?></XMP>
	<div style="width:90%;height:300px;overflow:scroll;"><XMP><?php echo ($this->exec( 'git log --pretty=format:"%h%x09%an%x09%ad%x09%s"' )); ?></XMP></div><XMP>
		<?php
		$status = ($this->exec( 'git status -s' ));
		if ( !strlen( $status ) ) {
			_e( 'Изменений в файлах нет' );
			echo "\n\n";
			echo $this->exec( 'git status' );
		} else {
			$mods = array(
				'D' => _( 'Удалены файлы' ),
				'M' => _( 'Изменены файлы' ),
				'??' => ('Добавлены файлы')
			);
			foreach ( $mods as $k => $value ) {
				$stat = 'git status -s | awk \'{if ($1 == "' . $k . '") print $2}\'';
				$res = $this->exec( $stat );
				if ( strlen( $res ) ) {
					?></XMP><h2><?php echo $value; ?></h2><XMP><?php
							echo $res;
						}
					}
				}
				?></XMP><?php if ( strlen( $status ) && is_admin() && $_GET['commit'] && strpos( get_bloginfo( 'admin_email' ), '@iwtech.ru' ) ) { ?>
		<form name = "iwt-form" method="post">
			<input type="hidden" name="do_submit" value="<?php echo wp_create_nonce( 'do_submit' ); ?>"/>
			<input type = "submit" value = "Зафикисировать изменения" class = "button button-primary"/>
		</form>
		<?php
	}
} else {
	_e( "Включите в настройках создание репозитория" );
}
