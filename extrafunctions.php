<?php
require('includes/functions_class.php');
$theme = new cw_theme('cweb theme');
$theme->delete_option('required_plugins');
$plugins = array(
    'Duplicate Post', 'Maintenance', 'Regenerate Thumbnails',
    'Media Library Categories', 'TinyMCE Advanced', 'cyr3lat',
    'WP Lightbox 2','Breadcrumb NavXT','Post type archive in menu'
);
foreach ($plugins as $plugin) {
    $theme->update_option('required_plugins', $plugin, $plugin);
}
$theme->disable_comments();
$fonts = array('http://fonts.googleapis.com/css?family=Ubuntu+Condensed&subset=cyrillic-ext,latin',
    'http://fonts.googleapis.com/css?family=Roboto&subset=latin,cyrillic-ext',
    $theme->themedir.'/assets/fonts/ocraextended.css');
foreach ($fonts as $font) {
    $theme->update_option('styles', $font, $font);
}
$theme->update_option('menu_defaults', 'container',false);
$theme->update_option('menu_defaults', 'container_class',false);
$theme->update_option('menu_defaults', 'menu_class','nav nav-pills');

//$theme->delete_option('required_plugins');
