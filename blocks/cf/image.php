<label for="<?php $params['field_name']; ?>">
    <?php echo $params['value']['label']; ?><br/>
    <?php
    if (strlen($params['field_value'])) {
        echo $this->get_thumb($params['field_value']);
    }
    ?>
    <input id="<?php echo $this->sanitize($params['field_name']); ?>" type="hidden" class="upload_image" name="<?php echo $params['field_name']; ?>" value="<?php echo $params['field_value']; ?>">
    <input type="button" value="<?php _e('Убрать');?>" class="iwt_image_remove button"/>
</label>
<br/>

<input id="<?php echo $this->sanitize($params['field_name']); ?>_button" type="button" value="<?php _e(Загрузить); ?>" class="upload_image_button" />
<?php
wp_nonce_field('_' . $params['name'], '_' . $params['name']);
